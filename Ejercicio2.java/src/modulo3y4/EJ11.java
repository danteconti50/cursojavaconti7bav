package modulo3y4;

import java.util.Scanner;

public class EJ11 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		char letra;
		
		System.out.print("Ingrese una letra ");
		letra = escaner.next().charAt(0);
		
		if(letra=='a'||letra=='e'||letra=='i'||letra=='o'||letra=='u') {
			System.out.println(letra+" es vocal");
		}
		else {
			System.out.println(letra+" es consonante");
		}
		escaner.close();
	}

}
