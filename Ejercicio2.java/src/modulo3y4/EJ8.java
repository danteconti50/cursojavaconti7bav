package modulo3y4;

import java.util.Scanner;

public class EJ8 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int Jg1, Jg2;
		
		System.out.println("Piedra Papel Tijeras ");
		System.out.println("   (1)   (2)    (3) ");
		
		System.out.print("Jugador 1 ");
		Jg1 = escaner.nextInt();
		System.out.print("Jugador 2 ");
		Jg2 = escaner.nextInt();
		
		if(Jg1==0) {
			if(Jg2==1) {
				System.out.println("Jugador 2, GANASTE");
			}
			else if(Jg2==2) {
				System.out.println("Jugador 1, GANASTE");
			}
			else {
				System.out.println("Empataron");
			}
		}
		else if(Jg1==1) {
			if(Jg2==0) {
				System.out.println("Jugador 1, GANASTE");
			}
			else if(Jg2==2) {
				System.out.println("Jugador 2, GANASTE");
			}
			else {
				System.out.println("Empataron");
			}
		}
		else if(Jg1==2) {
			if(Jg2==0) {
				System.out.println("Jugador 2, GANASTE");
			}
			else if(Jg2==1) {
				System.out.println("Jugador 1, GANASTE");
			}
			else {
				System.out.println("Empataron");
			}
		}
		escaner.close();
	}

}