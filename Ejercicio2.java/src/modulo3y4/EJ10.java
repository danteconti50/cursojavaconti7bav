package modulo3y4;

import java.util.Scanner;

public class EJ10 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int numero1, numero2, numero3;
		
		System.out.print("Ingrese su primer n�mero ");
		numero1 = escaner.nextInt();
		System.out.print("Ingrese su segundo n�mero "); 
		numero2 = escaner.nextInt();
		System.out.print("Ingrese su tercer n�mero ");
		numero3 = escaner.nextInt();
		
		if(numero1==numero2 && numero2==numero3) {
			System.out.println("Los tres n�meros son iguales");
		}
		else if(numero1>numero2 && numero2==numero3) {
			System.out.println("El ("+numero1+")es el mayor");
		}
		else if(numero2>numero3 && numero1==numero3) {
			System.out.println("El ("+numero2+") es el mayor");
		}
		else if(numero3>numero1 && numero1==numero2) {
			System.out.println("El ("+numero3+") es el mayor");
		}
		else if(numero1==numero2 && numero1>numero3) {
			System.out.println("El ("+numero1+") y ("+numero2+") son los mayores");
		}
		else if(numero1==numero3 && numero1>numero2) {
			System.out.println("El ("+numero1+") y ("+numero3+") son los mayores");
		}
		else if(numero2==numero3 && numero2>numero1) {
			System.out.println("El ("+numero2+") y ("+numero3+") son los mayores");
		}
		else if(numero1>numero2 && numero2>numero3) {
			System.out.println("El ("+numero1+") n�mero es el mayor");
		}
		else if(numero1<numero2 && numero2<numero3) {
			System.out.println("El ("+numero3+") es el mayor");
		}
		else if(numero1<numero2 && numero2>numero3) {
			System.out.println("El ("+numero2+") es el mayor");
		}
		escaner.close();
		
	}

}
