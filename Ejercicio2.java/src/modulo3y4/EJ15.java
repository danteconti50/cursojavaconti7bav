package modulo3y4;

import java.util.Scanner;

public class EJ15 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		char clase;

		System.out.print("Coloque la categor�a de su veh�culo ");
		clase = scan.next().charAt(0);
		
		switch(clase) {
		case 'a': case 'A':
			System.out.println("Su veh�culo clase \""+clase+"\" tiene ");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			break;
		case 'b': case 'B':
			System.out.println("Su veh�culo clase \""+clase+"\" tiene ");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			System.out.println("Sistema de cerradura centralizada");
			System.out.println("Sistema de aire acondicionado");
			break;
		case 'c': case 'C':
			System.out.println("Su veh�culo clase \""+clase+"\" tiene");
			System.out.println("4 ruedas");
			System.out.println("Un motor");
			System.out.println("Sistema de cerradura centralizada");
			System.out.println("Sistema de aire acondicionado");
			System.out.println("Sistema de Airbag");
			break;
		default:
			System.out.println("La clase\""+clase+"\" es inexistente");
			break;
		}
		
		scan.close();

	}

}