package modulo3y4;

import java.util.Scanner;

public class EJ16 {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner escaner = new Scanner(System.in);
		
		int numero, a, resultado;
		resultado = 0;
		
		System.out.println("�De que numero desea saber la tabla?");
		numero = escaner.nextInt();
		
		for(a=1; a<=10; a++) {
			resultado=numero*a;
			System.out.println(numero+" * "+a+" = "+resultado);
		}
		escaner.close();
		
	}

}
