package modulo3y4;

import java.util.Scanner;

public class EJ6 {

	public static void main(String[] args) {
		
		int grado;
		
		Scanner escaner = new Scanner(System.in);
		
		System.out.print("Ingrese los a�os de estudio de la persona");
		grado = escaner.nextInt();
		
		if(grado>=0) {
			if(grado==0) {
				System.out.println("Est� cursando el jard�n de infantes");
			}
			else if(grado>12){
				System.out.println("Est� cursando una carrera");
			}
			else if(grado<=12){
				if(grado>=7){
					System.out.println("Est� cursando la secundaria");
				}
				else if(grado<=6) {
					if(grado>=1) {
						System.out.println("Est� cursando la primaria");
					}
					else {
						System.out.println("No estudia o ingres� un a�o incorrecto");
					}
				}
			}
		}
		else {
			System.out.println("No estudia o ingres� un a�o incorrecto");
		}
		escaner.close();

	}

}
