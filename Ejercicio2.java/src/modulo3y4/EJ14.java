package modulo3y4;

import java.util.Scanner;

public class EJ14 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int lugar;
		
		System.out.print("�En que puesto sali� en el torneo? ");
		lugar = escaner.nextInt();
		
		switch(lugar) {
		case 1:
			System.out.println("Usted termin� en "+lugar+"er puesto. Se llev� la medalla de Oro papu.");
			break;
		case 2:
			System.out.println("Usted termin� en "+lugar+"do puesto. Se llev� la medalla de Plata rey.");
			break;
		case 3:
			System.out.println("Usted termin� en "+lugar+"er puesto. Se llev� la medalla de Bronce.");
			break;
		default:
			System.out.println("Qued� debajo de los 3 primeros lugares");
		}
		escaner.close();

	}

}