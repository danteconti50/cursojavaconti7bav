package modulo3y4;

import java.util.Scanner;

public class EJ2 {

	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int numero, resto;
		
		System.out.print("Ingres� un n�mero capo");
		numero = escaner.nextInt();
		
		resto = numero%2;

		if(resto==0) {
			System.out.println("El n�mero ingresado es par");
		}
		else{
			System.out.println("El n�mero ingresado es impar");
		}
		
		escaner.close();

	}

}
