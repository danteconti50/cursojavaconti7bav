package modulo3y4;

import java.util.Scanner;

public class EJ12 {
	
	public static void main(String[] args) {
		
		Scanner escaner = new Scanner(System.in);
		
		int numero;
		
		System.out.print("Ingrese su n�mero ");
		numero = escaner.nextInt();
		
		if(numero>=1 && numero<=12) {
			System.out.println("El n�mero que ingres� est� dentro de la primer docena");
		}
		else if(numero>=13 && numero<=24) {
			System.out.println("El n�mero que ingres� est� dentro de la segunda docena");
		}
		else if(numero>=25 && numero<=36) {
			System.out.println("El n�mero que ingres� est� dentro de la tercer docena");
		}
		else if(numero<1 || numero>36) {
			System.out.println("El n�mero que ingres� est� fuera de rango");
		}
		escaner.close();
	}
}
